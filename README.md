# Microservicio de configuraciones

_MS  que expone las url y otras configuraciones que pueden ser consumidas__por el ms cliente_

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.

### Pre-requisitos 📋

_necesitas tener instalado **JAVA 8**   y  **Maven  3.6**_

```
java  =  https://openjdk.java.net/install/
maven =  https://maven.apache.org/ref/3.6.3/
```

### Instalación 🔧

_Dar doble click sobre el archivo install.cmd_

_**Si no**, lo puedes hacer pasos por paso  dentro del directorio **config-server** como detallo  a continuacion,  dentro de la consola msdos o la terminal de linux_

```
mvn clean  &&  mvn package
```

ejecutar

```
java  -jar  target/*.jar
```

Probar la api

iniciar el explorador de su equipo Chrome o Firefox  y con la url siguiente.

```
http://localhost:8889/ms-client/dev
```
